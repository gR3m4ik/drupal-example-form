<?php

namespace Drupal\example_module\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
//use Drupal\Core\Ajax\RedirectCommand;
//use Drupal\Core\Url;

/**
 * Provides a Example module form.
 */
class ExampleForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'example_module_example';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form = [];
//    $form['#attached']['library'][] = 'example_module/example_module';

    //https://druki.ru/wiki/10/forms
    $request = \Drupal::request()->query->get('type');

    //Таксономия вида
    // Газ
    //    Кислород
    //Жидкость
    //    Вода
    //Металл
    //    Железо
    //    Литий
    //    Медь
    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('testss', 0, null, true);

    $terms_data = [];
    $options = [];
    foreach ($terms as $term) {
      foreach ($term->parents as $term_parent) {
        if ($term_parent == 0) {   // 0 - parent 1 lvl
//          $terms_data[$term->id()] = [
//            'type' => $term->get('name')->value,
//          ];

          $options[$term->id()] = $term->get('name')->value;
        } else {
          $terms_data[$term_parent][$term->id()] = [
            'name' => $term->get('name')->value,
            'min'  => $term->get('field_min')->value,
            'max'  => $term->get('field_max')->value
          ];
        }
      }
    }

    $form['container'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => 'example-container',
        ],
    ];

    $select = $form_state->getValue('term_select') ?? $request ?? array_key_first($options);
    $form['container']['term_select'] = [
      '#type' => 'select',                        // типизация элементов
      '#title' => t('Выбор'),
      '#default_value' => $select,
      '#options' => $options,
      '#ajax' => [                                // ajax реализация для формы - тут при свапе между опциями будет работать ajax и перестраиваться форма
        'callback' => '::refreshContainerForm',   // метод какой используем
        'wrapper'  => 'example-container',           // определяем где конкретно возвращаем это
      ],
    ];


    // Пример как можно вывести таблицу
    $header = [
      'name' => t('Название'),
//      'type' => t('Тип'),
      'min'  => t('Свойство 1'),
      'max'  => t('Свойство 2'),
    ];

    $rows = [];
//    dump($form_state->getValue('term_select'));
    foreach ($terms_data[$select] as $k_row => $row) {
      $rows[] = [$row['name'], $row['min'], $row['max']];
    }

    $form['container']['table'] = [
      '#type'   => 'table',
      '#header' => $header,
      '#rows'   => $rows,
    ];
    // End table

    // Навигация
    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Send'),
    ];

    return $form;
  }

//  /**
//   * {@inheritdoc}
//   */
//  public function validateForm(array &$form, FormStateInterface $form_state) {
//
//  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
//    $this->messenger()->addStatus($this->t('The message has been sent.'));

    $form_state->setRebuild();
  }

  /**
   * Ajax callback to refresh the form.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The updated form array.
   */
  public function refreshContainerForm(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();

    $response->addCommand(new HtmlCommand('#example-container', $form['container']));

//    // Build the URL with the updated query parameter
//    $url = Url::fromRoute('<current>');
//    $url->setOption('query', ['type' => $form_state->getValue('term_select')]);
//
//    // Update the browser URL with the new URL
//    $response->addCommand(new RedirectCommand($url->toString()));

    return $response;
  }

}
